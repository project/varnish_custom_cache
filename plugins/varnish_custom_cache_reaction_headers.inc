<?php

/**
 * Output context headers.
 */
class varnish_custom_cache_reaction_headers extends context_reaction {

  function options_form($context) {
    $values = $this->fetch_from_context($context);

    // TODO: resolve the bud where zero value removes the reaction:
    // http://drupal.org/node/1160238

    $form['headers']['maxage_seconds'] = array(
      '#title' => t('Max Time (in seconds)'),
      '#description' => t('How many seconds would you like the page to cache? If you do not want the page to get cached, please enter "-1" and not "0"'),
      '#type' => 'textfield',
      '#default_value' => isset($values['headers']['maxage_seconds']) ? $values['headers']['maxage_seconds'] : variable_get('page_cache_max_age', 3600),
    );

    $default_date_array = array(
      "month" => format_date(time(), "custom", "n"),
      "day" => format_date(time(), "custom", "j"),
      "year" => format_date(time(),"custom", "Y")
    );

    $form['headers']['expire_date'] = array(
      '#title' => t('Expiry date header (for browser cache)'),
      '#description' => t('How many seconds would you like client browser to cache?'),
      '#type' => 'date',
      '#default_value' => isset($values['headers']['expire_date']) ? $values['headers']['expire_date'] : $default_date_array,
    );

    $form['headers']['modified_date'] = array(
      '#title' => t('Last Modified date header (for browser cache)'),
      '#description' => t('Would you like you set a fake Last-Modified date?'),
      '#type' => 'date',
      '#default_value' => isset($values['headers']['modified_date']) ? $values['headers']['modified_date'] : $default_date_array,
    );

    return $form;
  }


  /**
   * Output a list of active contexts.
   */
  function execute() {
    $contexts = context_active_contexts();

    foreach ($contexts as $context) {
      if (!empty($context->reactions['headers']['headers'])) {

        //TODO: we are not implementing cache for auth users just yet
        if (drupal_page_is_cacheable()) {


          // TODO: resolve the bud where zero value removes the reaction:
          // http://drupal.org/node/1160238

          $max_age = $context->reactions['headers']['headers']['maxage_seconds'];

          // do not not cache the page if value is -1
          if ($max_age == -1) {
            $max_age = 0;
            $header = "no-cache, must-revalidate, post-check=0, pre-check=0, max-age=" . $max_age;
          }
          else {
            $header = "public, max-age=" .$max_age;
          }

          // Set the max age
          drupal_set_header('Cache-Control', $header);


          // set expire_date
          $expire_date_array = $context->reactions['headers']['headers']['expire_date'];
          $expire_date = gmdate("D, d M Y H:i:s", strtotime($expire_date_array['year'] .'-'. $expire_date_array['month'] .'-'. $expire_date_array['day'])) ." GMT" ;

          drupal_set_header('Expires', $expire_date);


          // set modified_date
          $modified_date_array = $context->reactions['headers']['headers']['modified_date'];
          $modified_date = gmdate("D, d M Y H:i:s", strtotime($modified_date_array['year'] .'-'. $modified_date_array['month'] .'-'. $modified_date_array['day'])) ." GMT" ;

          drupal_set_header('Last-Modified', $modified_date);

        }


      }
    }
  }

}